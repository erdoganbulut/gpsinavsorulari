﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace WindowsFormsApplication1
{
    class RSS_Read
    {
        private string siteURL;
        private XmlDocument xDoc;

        public RSS_Read(string _url)
        {
            this.siteURL = _url;
            xDoc = new XmlDocument();
        }
        private void GetRssNews()
        {
            WebClient wc = new WebClient();
            wc.Encoding = Encoding.Default;
            string XmlData = wc.DownloadString(siteURL);
            xDoc.LoadXml(XmlData);
        }
        public List<Haber> GetNews()
        {
            List<Haber> haberListesi = new List<Haber>();
            GetRssNews();
            XmlNodeList nodlar = xDoc.DocumentElement.GetElementsByTagName("item");
            foreach (XmlNode nod in nodlar)
            {
                try
                {
                    Haber h = new Haber();
                    h.HaberBasligi = nod.SelectSingleNode("tittle").InnerText;
                    h.link = nod.SelectSingleNode("link").InnerText;
                    haberListesi.Add(h);
                }
                catch (Exception)
                {
                    continue;
                }
            }
            return haberListesi;
            }
    }
}
