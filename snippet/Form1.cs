﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {

        // ÖNEMLİ ADOCon.snippedı oluşturmak için addview xmloluştur uzantısını .snippet yapılcak
        // <snippet></snippet> yazıp tab tab yapıyoruz sonra kaydetip tools Code Snippets Manager a basip C# seçiyoruz 
        //kaydediyoruz 

        public Form1()
        {
            InitializeComponent();
            webBrowser1.ScriptErrorsSuppressed = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textBox1.Text))
            {
                MessageBox.Show("Site url si boş geçilemez ");
            }
            else
            {
                listView1.Clear();
                RSS_Read reader = new RSS_Read(textBox1.Text);
                foreach  (Haber haber in reader.GetNews())
                {
                    ListViewItem lvi = new ListViewItem();
                    lvi.Text = haber.HaberBasligi;
                    lvi.SubItems.Add(haber.link);
                    listView1.Items.Add(lvi);         
                }
            }

        }

        private void listView1_DoubleClick(object sender, EventArgs e)
        {
            if (listView1.Items.Count >0)
            {
                webBrowser1.Navigate(listView1.SelectedItems[0].SubItems[1].Text);
            }
        }
    }
}
