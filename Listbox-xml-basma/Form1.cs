﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                SqlDataAdapter dap = new SqlDataAdapter("Select FirstName,LastName,BirthDate from Employees", "Server=UMUT-BILGISAYAR;Database=Northwind;trusted_connection=true");

                DataTable dt = new DataTable("Employees");// yeni bir datatable oluştur ve bunun adına employees de 
                dap.Fill(dt);
                dt.WriteXml("../../Employees.xml");// ana dizine çıkmak için ../../ kullanıyoruz
                MessageBox.Show("İşlem Başarılı");
                //Çalıştırdıktan sonra sağ üst köşede Show All Filesa tıklıyoruz 
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
