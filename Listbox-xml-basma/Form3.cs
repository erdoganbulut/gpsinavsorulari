﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace WindowsFormsApplication1
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {
           
        }

        private void Form3_Load(object sender, EventArgs e)
        {
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
           
            
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            comboBox1.Items.Clear();
            XmlDocument xdoc = new XmlDocument();
            string url = "http://www.tcmb.gov.tr/kurlar/today.xml";
            WebClient wc = new WebClient();
            wc.Encoding = Encoding.Default;
            string xmlData = wc.DownloadString(url);
            xdoc.LoadXml(xmlData);//loadXml dosyasının içine string bir ifade atmamız gerekiyor

            XmlNodeList kurlar = xdoc.DocumentElement.ChildNodes;
            foreach (XmlNode kur in kurlar)
            {
                string kurAdi = kur.ChildNodes[2].InnerText;
                comboBox1.Items.Add(kurAdi);
            }
        }
    }
}
