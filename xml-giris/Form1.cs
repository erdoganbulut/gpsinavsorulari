﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace XML_GirisDers1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                XmlDocument xDoc = new XmlDocument();
                XmlDeclaration xDec = xDoc.CreateXmlDeclaration("1.0", "utf-8", string.Empty);
                xDoc.AppendChild(xDec);
                XmlElement root = xDoc.CreateElement("Filmler");
                XmlElement film = xDoc.CreateElement("Film");

                XmlAttribute id = xDoc.CreateAttribute("FilmID");
                id.InnerText = "1";
                film.Attributes.Append(id);

                XmlElement ad = xDoc.CreateElement("FilmAdi");
                ad.InnerText = "Esaretin Bedeli";
                film.AppendChild(ad);

                XmlElement YapimYili = xDoc.CreateElement("YapimYili");
                YapimYili.InnerText ="1994";
                film.AppendChild(YapimYili);

                XmlElement puan = xDoc.CreateElement("puani");
                puan.InnerText = "9.4";
                film.AppendChild(puan);

                XmlAttribute id2 = xDoc.CreateAttribute("FilmID");
                id2.InnerText = "2";
                film.Attributes.Append(id2);

                XmlElement ad1 = xDoc.CreateElement("FilmAdi");
                ad1.InnerText = "Umut";
                film.AppendChild(ad1);

                XmlElement YapimYili1 = xDoc.CreateElement("YapimYili");
                YapimYili1.InnerText = "2009";
                film.AppendChild(YapimYili1);

                XmlElement puan1 = xDoc.CreateElement("puani");
                puan1.InnerText = "7.5";
                film.AppendChild(puan1);

                XmlAttribute id3 = xDoc.CreateAttribute("FilmID");
                id3.InnerText = "3";
                film.Attributes.Append(id3);

                XmlElement ad2 = xDoc.CreateElement("FilmAdi");
                ad2.InnerText = "Diriliş";
                film.AppendChild(ad2);

                XmlElement YapimYili2 = xDoc.CreateElement("YapimYili");
                YapimYili2.InnerText = "2016";
                film.AppendChild(YapimYili2);

                XmlElement puan2 = xDoc.CreateElement("puani");
                puan2.InnerText = "7.9";
                film.AppendChild(puan2);

                XmlAttribute id4 = xDoc.CreateAttribute("FilmID");
                id4.InnerText = "4";
                film.Attributes.Append(id4);

                XmlElement ad3 = xDoc.CreateElement("FilmAdi");
                ad3.InnerText = "BirdMan";
                film.AppendChild(ad3);

                XmlElement YapimYili3 = xDoc.CreateElement("YapimYili");
                YapimYili3.InnerText = "2015";
                film.AppendChild(YapimYili3);

                XmlElement puan3 = xDoc.CreateElement("puani");
                puan3.InnerText = "8.2";
                film.AppendChild(puan3);

                root.AppendChild(film);
                xDoc.AppendChild(root);
                xDoc.Save("../../Filmler.xml");
                MessageBox.Show("Dosya yazdırıldı");
            }
            catch (Exception ex )
            {

                MessageBox.Show(ex.Message);
            }
            
        }
    }
}
