﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace _4._4._16_görsel_3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DateTime baslangic = DateTime.Now;
            SqlConnection cnn = new SqlConnection("Server=USER\\SQLEXPRESS;Database=Northwind;trusted_connection =true");
            SqlCommand cmd = new SqlCommand("Select * from Orders", cnn);
            if (cnn.State == ConnectionState.Closed) cnn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while(dr.Read())
                {
                    listBox1.Items.Add(dr["OrderID"]);
                }
            }
            DateTime bitis = DateTime.Now;
            TimeSpan fark = bitis - baslangic;
            lblConnected.Text = fark.Milliseconds.ToString();
            
        }
        
        private void button2_Click(object sender, EventArgs e)
        {
            DateTime baslangic = DateTime.Now;
            SqlDataAdapter dap= new SqlDataAdapter("Select * from Orders","Server=USER\\SQLEXPRESS;Database=Northwind;trusted_connection =true");
            DataTable dt=new DataTable();
            dap.Fill(dt);
            listBox2.DataSource=dt;
            listBox2.DisplayMember="OrderID";
            DateTime bitis = DateTime.Now;
            TimeSpan fark = bitis - baslangic;
            lblConnected.Text = fark.Milliseconds.ToString();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            DateTime baslangic = DateTime.Now;
            NorthwindEntities1 db = new NorthwindEntities1();
            listBox3.DataSource = db.Orders.ToList();
            //var a = ( from i in db.orders select i.ordersID).tolist(); üstteki kod aynı işi görüyor 
            // listbox3.datasource = a;
            listBox3.DisplayMember = "OrderID";
            DateTime bitis = DateTime.Now;
            TimeSpan fark = bitis - baslangic;
            lblEntity.Text = fark.Milliseconds.ToString();

        }
    }
}
