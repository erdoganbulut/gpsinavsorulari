﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _4._4._16_görsel_3
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }
        TelRehberiDB3Entities db = new TelRehberiDB3Entities();
        private void button1_Click(object sender, EventArgs e)
        {
           if(string.IsNullOrEmpty(txtAd.Text) || string.IsNullOrEmpty(txtSoyad.Text) || string.IsNullOrEmpty(txtTelefonNo.Text))
           {
               MessageBox.Show("Bilgileri eksiksiz doldurunuz.");
               return;
           }
           Kisiler k = new Kisiler();
           k.KisiAdi = txtAd.Text;
           k.KisiSoyadi = txtSoyad.Text;
           k.TelefonNo = txtTelefonNo.Text;

           db.Kisiler.Add(k);
           db.SaveChanges();
           VerileriGetir();
           
        }
        private void groupBox1_Enter(object sender, EventArgs e)
        {
            
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            VerileriGetir();   
        }

        private void VerileriGetir()
        {
            listBox1.DataSource = db.Kisiler.ToList();
            listBox1.DisplayMember = "KisiAdi";
            
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

       Kisiler guncellenecek;
       private void listBox1_DoubleClick(object sender, EventArgs e)
        {
           if (listBox1.SelectedItems.Count>0)
           {
               guncellenecek = (Kisiler)listBox1.SelectedItem;
               txtAd.Text = guncellenecek.KisiAdi;
               txtSoyad.Text = guncellenecek.KisiSoyadi;
               txtTelefonNo.Text = guncellenecek.TelefonNo;
           }

        }

       private void button2_Click(object sender, EventArgs e)
       {
           if (string.IsNullOrEmpty(txtAd.Text) || string.IsNullOrEmpty(txtSoyad.Text) || string.IsNullOrEmpty(txtTelefonNo.Text))
           {
               MessageBox.Show("Bilgileri eksiksiz doldurunuz.");
               return;
           }

           guncellenecek.KisiAdi = txtAd.Text;
           guncellenecek.KisiSoyadi = txtSoyad.Text;
           guncellenecek.TelefonNo = txtTelefonNo.Text;

           db.SaveChanges();
           VerileriGetir();
       }

       private void button3_Click(object sender, EventArgs e)
       {
           if (listBox1.SelectedItems.Count>0)
           {
               Kisiler silinecek = (Kisiler)listBox1.SelectedItem;
               db.Kisiler.Remove(silinecek);
               db.SaveChanges();
               VerileriGetir();
           }
       }
    }
}
