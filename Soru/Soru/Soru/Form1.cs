﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace Soru
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            NorthwindEntities entity = new NorthwindEntities();
            XmlDocument xmldoc = new XmlDocument();
            XmlDeclaration xmldec = xmldoc.CreateXmlDeclaration("1.0", "utf-8", string.Empty);
            xmldoc.AppendChild(xmldec);
            XmlElement root = xmldoc.CreateElement("Employees");
            foreach (var item in entity.Employees.ToList())
            {
                XmlElement employee = xmldoc.CreateElement("Employee");
                XmlAttribute id = xmldoc.CreateAttribute("EmployeeID");
                id.InnerText = item.EmployeeID.ToString();
                employee.Attributes.Append(id);

                XmlElement ad = xmldoc.CreateElement("FirstName");
                ad.InnerText = item.FirstName.ToString();
                employee.AppendChild(ad);

                XmlElement soyad = xmldoc.CreateElement("LastName");
                soyad.InnerText = item.LastName.ToString();
                employee.AppendChild(soyad);

                root.AppendChild(employee);
            }
            xmldoc.AppendChild(root);
            xmldoc.Save("../../Employees.xml");
            MessageBox.Show("Dosya yazdırıldı");
        }
    }
}
