﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace Soru
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            XmlDocument xDoc = new XmlDocument();
            xDoc.Load("../../Employees.xml");
            XmlNodeList calisanlar = xDoc.DocumentElement.ChildNodes;
            foreach (XmlNode calisan in calisanlar)
            {
                string ad = calisan.ChildNodes[0].InnerText;
                string soyad = calisan.ChildNodes[1].InnerText;
                listBox1.Items.Add(string.Format("Ad :{0} Soyad :{1}", ad, soyad));
            }
        }
    }
}
